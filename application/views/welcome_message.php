<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to backend service</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

</head>
<body>

<nav>
    <div class="nav-wrapper indigo">
        <span class="brand-logo center">backend description</span>
    </div>
</nav>


<div class="row">
    <div class="col s8 offset-s2">
        <div class="card">
            <div class="card">
                <ul class="collection">
                    <li class="collection-item">
                        <h5>CAR</h5><hr>
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn blue col s1">GET</span>
                                    <div class="col s11">
                                        <b>/index.php/car</b> &nbsp; return collection of cars.
                                    </div>
                                </div>
                                <div class="collapsible-body">
                                    <h5>available parameters</h5>
                                    <div>
                                        <table>
                                            <thead>
                                                <th>name</th>
                                                <th>description</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>id</td>
                                                    <td>id of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>plate_number</td>
                                                    <td>plate number of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>kilometers_count</td>
                                                    <td>car with exactly this kilometers count</td>
                                                </tr>
                                                <tr>
                                                    <td>kilometers_countgt</td>
                                                    <td>car with kilometers count greater than a number</td>
                                                </tr>
                                                <tr>
                                                    <td>kilometers_countlt</td>
                                                    <td>car with kilometers count lower than a number</td>
                                                </tr>
                                                <tr>
                                                    <td>ref_insurance</td>
                                                    <td>insurance reference for a car</td>
                                                </tr>
                                                <tr>
                                                    <td>last_control</td>
                                                    <td>car with last control at this date</td>
                                                </tr>
                                                <tr>
                                                    <td>last_controlgt</td>
                                                    <td>car with last control greater than this date</td>
                                                </tr>
                                                <tr>
                                                    <td>last_controllt</td>
                                                    <td>car with last control lower than this date</td>
                                                </tr>
                                                <tr>
                                                    <td>disponibility</td>
                                                    <td>disponibility of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>status</td>
                                                    <td>status of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>brand</td>
                                                    <td>brand of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>model</td>
                                                    <td>model of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>fuel_type</td>
                                                    <td>fuel_type of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>places</td>
                                                    <td>number of place in a car</td>
                                                </tr>
                                                <tr>
                                                    <td>engine</td>
                                                    <td>engine of a car</td>
                                                </tr>
                                                <tr>
                                                    <td>category</td>
                                                    <td>category of a car</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <div class="btn green col s1">POST</div>
                                    <div class="col s11">
                                        <b>/index.php/car</b> create a new car.
                                    </div>
                                </div>
                                <div class="collapsible-body">
                                    <pre>
    {
        "model_id": "int",
        "plate_number": "string",
        "kilometers_count": "int",
        "last_control": "date",
        "ref_insurance": "string",
        "disponibility": "int",
        "status": "string"
    }
                                    </pre>
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <div class="btn yellow darken-3 col s1">PUT</div>
                                    <div class="col s11"> <b>/index.php/car</b> update an existent car. </div>
                                </div>
                                <div class="collapsible-body">
                                    <pre>
    {
        "model_id": "int",
        "plate_number": "string",
        "kilometers_count": "int",
        "last_control": "date",
        "ref_insurance": "string",
        "disponibility": "int",
        "status": "string"
    }
                                    </pre>
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <div class="btn red col s1">DELETE</div>
                                    <div class="col s11"> <b>/index.php/car</b> suppress an existent car</div>
                                </div>
                                <div class="collapsible-body">
                                    <div>{ "id": "int" }</div>
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <div class="btn blue col s1">GET</div>
                                    <div class="col s11"><b>/index.php/car/metrics</b> return metrics for all car.</div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="collection-item">
                        <h5>GROUP</h5><hr>
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn blue">&nbsp;GET&nbsp;</span> &nbsp;&nbsp; <b>/index.php/group</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn green">POST</span> &nbsp;&nbsp; <b>/index.php/group</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn yellow darken-3">&nbsp;PUT&nbsp;</span> &nbsp;&nbsp; <b>/index.php/group</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn red">DELETE</span> &nbsp;&nbsp; <b>/index.php/group</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                        </ul>
                    </li>
                    <li class="collection-item">
                        <h5>MODEL</h5><hr>
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn blue">&nbsp;GET&nbsp;</span> &nbsp;&nbsp; <b>/index.php/model</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn green">POST</span> &nbsp;&nbsp; <b>/index.php/model</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn yellow darken-3">&nbsp;PUT&nbsp;</span> &nbsp;&nbsp; <b>/index.php/model</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn red">DELETE</span> &nbsp;&nbsp; <b>/index.php/model</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                        </ul>
                    </li>
                    <li class="collection-item">
                        <h5>RESERVATION</h5><hr>
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn blue">&nbsp;GET&nbsp;</span> &nbsp;&nbsp; <b>/index.php/reservation</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn green">POST</span> &nbsp;&nbsp; <b>/index.php/reservation</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn yellow darken-3">&nbsp;PUT&nbsp;</span> &nbsp;&nbsp; <b>/index.php/reservation</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn red">DELETE</span> &nbsp;&nbsp; <b>/index.php/reservation</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                        </ul>
                    </li>
                    <li class="collection-item">
                        <h5>RIGHT</h5><hr>
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn blue">&nbsp;GET&nbsp;</span> &nbsp;&nbsp; <b>/index.php/right</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn green">POST</span> &nbsp;&nbsp; <b>/index.php/right</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn yellow darken-3">&nbsp;PUT&nbsp;</span> &nbsp;&nbsp; <b>/index.php/right</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn red">DELETE</span> &nbsp;&nbsp; <b>/index.php/right</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                        </ul>
                    </li>
                    <li class="collection-item">
                        <h5>USER</h5><hr>
                        <ul class="collapsible">
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn blue">&nbsp;GET&nbsp;</span> &nbsp;&nbsp; <b>/index.php/user</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn green">POST</span> &nbsp;&nbsp; <b>/index.php/user</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn yellow darken-3">&nbsp;PUT&nbsp;</span> &nbsp;&nbsp; <b>/index.php/user</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                            <li>
                                <div class="collapsible-header">
                                    <span class="btn red">DELETE</span> &nbsp;&nbsp; <b>/index.php/user</b>
                                </div>
                                <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<footer class="page-footer indigo">
    <div class="footer-copyright right-align">
        <div class="container">
            <p>Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
        </div>
    </div>
</footer>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script>M.AutoInit();</script>
</body>
</html>