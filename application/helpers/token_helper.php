<?php

/**
 * permet de connaitre si un utilisateur possède un droit un particulier.
 * @param string $table la table sur laquel l'utilisateur veut agire
 * @param string $method le verb utiliser pour la reqêtte http
 * @param string $token le token de l'utilisateur
 * @return bool
 */
function addRight($table, $method, $token) {
  foreach ($token->rights as $right){
    if ($right->method == $method && $right->table == $table)
      return true;
  }
  return false;
}