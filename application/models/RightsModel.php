<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 11:29
 */

/**
 * Class RightsModel model pour l'entité
 */
class RightsModel extends CI_Model {

  /**
   * récupère tous les éléments
   * @param array $criterias filtres à appliquer pour la requêtte sql
   * @return mixed
   */
  public function getAll($criterias = array()) {
    $req = $this->db->select('*')->from('rights');
    if (sizeof($criterias) > 0)
      $req = $this->applyCriterias($criterias, $req);
    return $req->get()->result();
  }

  /**
   * récupère un élément
   * @param int $id id de l'éléments
   * @return mixed
   */
  public function getOne($id) {
    return $this->db->select('*')->from('rights')->where('id', $id)->get()->result();
  }

  /**
   * supprime un élément
   * @param int $id id de l'élément
   */
  public function delete($id) {
    $this->db->delete('rights', array('id' => $id));
  }

  /**
   * modifie un élément
   * @param array $data donné à mettre à jour
   */
  public function update($data) {
    $this->db->where('id', $data['id'])->update('rights', $data);
  }

  /**
   * créer un élément
   * @param array $data données pour la création
   */
  public function create($data) {
    $this->db->insert('rights', $data);
  }

  /**
   * applique les filtres choisies pour la reqêtte sql
   * @param array $criterias filtres pour la requêttes
   * @param string $request requêtte sql
   * @return string reqêtte sql avec les filtres
   */
  public function applyCriterias($criterias, $request) {
    $allow = array('right');
    for ($i = 0; $i < sizeof($allow); $i++) {
      if (in_array($allow[$i], array_keys($criterias))) {
        $request->where($allow[$i], $criterias[$allow[$i]]);
      }
    }
    return $request;
  }
}