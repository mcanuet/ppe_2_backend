<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 12:05
 */

/**
 * Class CarsModel model pour l'entité voiture
 */
class CarsModel extends CI_Model {

  /**
   * récupère tous les éléments
   * @param array $criterias filtres à appliquer pour la requêtte sql
   * @return mixed
   */
    public function getAll($criterias = array()) {
        $req = $this->db->select('cars.*')->from('cars');
        if (sizeof($criterias) > 0)
            $req = $this->applyCriterias($criterias, $req);
        return $req->get()->result();
    }

  /**
   * récupère un élément
   * @param int $id id de l'éléments
   * @return mixed
   */
    public function getOne($id) {
        return $this->db->select('*')->from('cars')->where('cars.id', $id)->get()->result();
    }

  /**
   * supprime un élément
   * @param int $id id de l'élément
   */
    public function delete($id) {
        $this->db->delete('cars', array('id' => $id));
    }

  /**
   * modifie un élément
   * @param array $data donné à mettre à jour
   */
    public function update($data) {
        $this->db->where('id', $data['id'])->update('cars', $data);
    }

  /**
   * créer un élément
   * @param array $data données pour la création
   */
    public function create($data) {
        $this->db->insert('cars', $data);
    }

  /**
   * permet de trouver une voiture libre pour une reservation
   * @param int $modelId id du model
   * @param string $start date de début de la réservation
   * @param string $end date de fin de la reservation
   * @return mixed
   */
    public function findFreeCar($modelId, $start, $end) {
        return $this->db->select('findCar('.$start.', '.$end.', '.$modelId.') as car_id')->get()->result();
    }

  /**
   * applique les filtres choisies pour la reqêtte sql
   * @param array $criterias filtres pour la requêttes
   * @param string $request requêtte sql
   * @return string reqêtte sql avec les filtres
   */
    public function applyCriterias($criterias, $request) {
        $request->join('models', 'cars.model_id = models.id');

        $allow = array('model_id', 'plate_number', 'kilometers_count', 'kilometers_countgt', 'kilometers_countlt', 'ref_insurance', 'last_control',
          'last_controlgt', 'last_controllt', 'disponibility', 'status', 'brand', 'model', 'fuel_type', 'places', 'engine', 'category');
        for ($i = 0; $i < sizeof($allow); $i++) {
            if (in_array($allow[$i], array_keys($criterias))) {
                if (substr($allow[$i], strlen($allow[$i])-2) === 'gt' ) {
                    $request->where(substr($allow[$i], 0,strlen($allow[$i])-2).'>', $criterias[$allow[$i]]);
                } elseif (substr($allow[$i], strlen($allow[$i])-2) === 'lt' ) {
                    $request->where(substr($allow[$i], 0,strlen($allow[$i])-2).'<', $criterias[$allow[$i]]);
                } else {
                    $request->where($allow[$i], $criterias[$allow[$i]]);
                }
            }
        }
        return $request;
    }
}