<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 11:29
 */

/**
 * Class GroupModel model pour l'entité group
 */
class GroupModel extends CI_Model {

  /**
   * récupère tous les éléments
   * @param array $criterias filtres à appliquer pour la requêtte sql
   * @return mixed
   */
  public function getAll($criterias = array()) {
    $req = $this->db->select('groups.*')->from('groups');
    if (sizeof($criterias) > 0)
      $req = $this->applyCriterias($criterias, $req);
    return $req->get()->result();
  }

  /**
   * récupère un élément
   * @param int $id id de l'éléments
   * @return mixed
   */
  public function getOne($id) {
    return $this->db->select('*')->from('groups')->where('id', $id)->get()->result();
  }

  /**
   * supprime un élément
   * @param int $id id de l'élément
   */
  public function delete($id) {
    $this->db->delete('groups', array('id' => $id));
  }

  /**
   * modifie un élément
   * @param array $data donné à mettre à jour
   */
  public function update($data) {
    $this->db->where('id', $data['id'])->update('groups', $data);
  }

  /**
   * créer un élément
   * @param array $data données pour la création
   * @return array l'élément créer
   */
  public function create($data) {
    $this->db->insert('groups', $data);
    return $this->db->select('id')->from('groups')->where('name', $data['name'])->get()->result();
  }

  /**
   * applique les filtres choisies pour la reqêtte sql
   * @param array $criterias filtres pour la requêttes
   * @param string $request requêtte sql
   * @return string reqêtte sql avec les filtres
   */
  public function applyCriterias($criterias, $request) {
    $allow = array('name');
    for ($i = 0; $i < sizeof($allow); $i++) {
      if (in_array($allow[$i], array_keys($criterias))) {
        $request->where($allow[$i], $criterias[$allow[$i]]);
      }
    }
    return $request;
  }
}