<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 11:29
 */

/**
 * Class RightsGroupModel model pour l'entité group_model
 */
class RightsGroupModel extends CI_Model {

  /**
   * récupère les droits d'un groupe
   * @param int $group_id l'identifiant du group
   * @return mixed
   */
  public function rightsForGroup($group_id){
    return $this->db->select('id_right')->from('rights_group')->where('id_group', $group_id)->get()->result();
  }

  /**
   * récupère les groupes qui possède un droit en particulier
   * @param int $right_id l'identifiant du droit
   * @return mixed
   */
  public function groupsForRight($right_id){
    return $this->db->select('id_group')->from('rights_group')->where('id_rights', $right_id)->get()->result();
  }

  /**
   * créer un groupe
   * @param array $data données pour la création
   */
  public function create($data){
    $this->db->insert('rights_group', $data);
  }

  /**
   * supprime un group
   * @param int $id id du group
   */
  public function deleteGroup($id){
    $this->db->delete('rights_group', array('id_group' => $id));
  }

  /**
   * supprime un droit pour un groupe
   * @param int $id_right l'identifiant du droit
   * @param int $id_group l'identifiant du group
   */
  public function deleteRight($id_right, $id_group){
    $this->db->delete('rights_group', array('id_right' => $id_right, 'id_group' => $id_group));
  }


  /**
   * modifie un groupe
   * @param array $data donné à mettre à jour
   */
  public function updateGroup($data){
    $this->db->where('id', $data['id'])->update('rights', $data);
  }
}