<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 11:36
 */

/**
 * Class ReservationsModel
 */
class ReservationsModel extends CI_Model {

  /**
   * récupère tous les éléments
   * @param array $criterias filtres à appliquer pour la requêtte sql
   * @return mixed
   */
    public function getAll($criterias = array()) {
        $req = $this->db->select('reservations.*')->from('reservations');
        if (sizeof($criterias) > 0)
            $req = $this->applyCriterias($criterias, $req);
        return $req->get()->result();
    }

  /**
   * récupère un élément
   * @param int $id id de l'éléments
   * @return mixed
   */
    public function getOne($id) {
        return $this->db
            ->select('*')->from('reservations')
            ->where('reservations.id', $id)->get()->result();
    }

  /**
   * supprime un élément
   * @param int $id id de l'élément
   */
    public function delete($id) {
        $this->db->delete('reservations', array('id' => $id));
    }

  /**
   * modifie un élément
   * @param array $data donné à mettre à jour
   */
    public function update($data) {
        $this->db->where('id', $data['id'])->update('reservations', $data);
    }

  /**
   * créer un élément
   * @param array $data données pour la création
   */
    public function create($data) {
        $this->db->insert('reservations', $data);
    }

  /**
   * applique les filtres choisies pour la reqêtte sql
   * @param array $criterias filtres pour la requêttes
   * @param string $request requêtte sql
   * @return string reqêtte sql avec les filtres
   */
    public function applyCriterias($criterias, $request) {
        $request->join('cars', 'reservations.car_id = cars.id')
            ->join('users', 'reservations.user_id = users.id');
        $allow = array('car_id', 'user_id', 'model_id', 'plate_number', 'kilometers_count', 'kilometers_countgt', 'kilometers_countlt',
            'ref_insurance', 'start', 'startgt', 'startlt', 'end', 'endgt', 'endlt');
        for ($i = 0; $i < sizeof($allow); $i++) {
            if (in_array($allow[$i], array_keys($criterias))) {
                if (substr($allow[$i], strlen($allow[$i])-2) === 'gt' ) {
                    $request->where(substr($allow[$i], 0,strlen($allow[$i])-2).'>', $criterias[$allow[$i]]);
                } elseif (substr($allow[$i], strlen($allow[$i])-2) === 'lt' ) {
                    $request->where(substr($allow[$i], 0,strlen($allow[$i])-2).'<', $criterias[$allow[$i]]);
                } else {
                    $request->where($allow[$i], $criterias[$allow[$i]]);
                }
            }
        }
        return $request;
    }
}