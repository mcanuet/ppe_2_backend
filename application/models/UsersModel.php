<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 11:29
 */

/**
 * Class CarsModel model pour l'entité user
 */
class UsersModel extends CI_Model {

  /**
   * récupère tous les éléments
   * @param array $criterias filtres à appliquer pour la requêtte sql
   * @return mixed
   */
    public function getAll($criterias = array()) {
        $req = $this->db->select('id, email, phone, zip_code, town, street_name, street_number, id_group')->from('users');
        if (sizeof($criterias) > 0)
            $req = $this->applyCriterias($criterias, $req);
        return $req->get()->result();
    }

  /**
   * récupère un élément
   * @param int $id id de l'éléments
   * @return mixed
   */
    public function getOne($id) {
        return $this->db->select('id, email, phone, zip_code, town, street_name, street_number, id_group')->from('users')->where('id', $id)->get()->result();
    }

  /**
   * supprime un élément
   * @param int $id id de l'élément
   */
    public function delete($id) {
        $this->db->delete('users', array('id' => $id));
    }

  /**
   * modifie un élément
   * @param array $data donné à mettre à jour
   */
    public function update($data) {
        $this->db->where('id', $data['id'])->update('users', $data);
    }

  /**
   * créer un élément
   * @param array $data données pour la création
   */
    public function create($data) {
      $data["password"] = sha1($data["password"]);
        $this->db->insert('users', $data);
    }

  /**
   * permet de verifier l'existence d'un utilisateur
   * @param string $login l'addresse email de l'utilisateur
   * @param string $pwd le mot de passe de l'utilisateur
   * @return array retourn l'utilisateur trouver avec les informations fournis
   */
    public function getByCredential($login, $pwd) {
      return $this->db->select('*')->from('users')
        ->where('email', $login)
        ->where('password', $pwd)
        ->get()->result();
    }

  /**
   * applique les filtres choisies pour la reqêtte sql
   * @param array $criterias filtres pour la requêttes
   * @param string $request requêtte sql
   * @return string reqêtte sql avec les filtres
   */
    public function applyCriterias($criterias, $request) {
        $allow = array('email', 'phone' ,'zip_code', 'town', 'street_name', 'street_number');
        for ($i = 0; $i < sizeof($allow); $i++) {
            if (in_array($allow[$i], array_keys($criterias))) {
                $request->where($allow[$i], $criterias[$allow[$i]]);
            }
        }
        return $request;
    }
}