<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 16:14
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Model permet de réaliser des actions sur les voitures
 */
class Model extends CI_Controller {

    private $method;
    private $postData;
    private $token;

    public function __construct() {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('ModelsModel');
        $this->load->model('CarsModel');
        $this->load->model('ReservationsModel');
        $this->load->model('UsersModel');
        $this->method = $this->input->server('REQUEST_METHOD');
        $this->postData = json_decode(trim(file_get_contents('php://input')), true);
        $this->token = $this->input->get_request_header('token', true);
    }

  /**
   * test le verb de la requêtte et utilise la methode adéquate
   * @return int code http de la reqêtte
   */
    public function index() {
        if (!in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'))) {
            return http_response_code(405);
        } else if (!addRight('models', $this->method, $this->jwt->decode($this->token)) ||
          !addRight('reservations', $this->method, $this->jwt->decode($this->token))){
          return http_response_code(403);
        } else {
          $this->{$this->method}();
        }
    }

  /**
   * permet de récupérer un ou plusieurs éléments
   */
    private function GET(){
        if (isset($_GET['id'])) {
            $models = $this->ModelsModel->getOne($_GET['id']);
        } else {
            $models = $this->ModelsModel->getAll($_GET);
        }
        $models[] = array('total items' => sizeof($models));
        echo json_encode($models);
    }

  /**
   * permet la création d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function POST(){
        if (array_diff(array('brand', 'model', 'fuel_type', 'image', 'places', 'engine', 'category'), array_keys($this->postData)) != null )
            throw new Exception("bad value send: brand, model, fuel_type, image, places, engine, category is required", 400);
        $this->ModelsModel->create($this->postData);
        return http_response_code(201);
    }

  /**
   * permet la modification d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function PUT(){
        if (!isset($this->postData['id']))
            throw new Exception('you need to set id in request body', 400);
        $this->ModelsModel->update($this->postData);
        return http_response_code(204);
    }

  /**
   * permet la suppression d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function DELETE(){
        if (!isset($_GET['id']))
            throw new Exception('you need to set parameter id', 400);
        $this->ModelsModel->delete($_GET['id']);
        return http_response_code(204);
    }

  /**
   * retourne les verbs possibles pour les requêtes
   */
    private function OPTIONS(){
        echo json_encode(array("allow methods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')));
    }

  /**
   * permet de reserver une voiture en fonction de son model
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    public function makeReservation() {
        if ($this->method !== 'POST')
            return http_response_code(405);
        if (array_diff(array('user_id', 'model_id', 'start', 'end'), array_keys($this->postData)) != null)
            throw new Exception("Wrong value you need to set user_id, model_id, start, end in request body");
        $start = date("Y-m-d H:i:s", strtotime($this->postData['start']));
        $end = date("Y-m-d H:i:s", strtotime($this->postData['end']));
        $carId = $this->CarsModel->findFreeCar($this->postData['model_id'], "'".$start."'", "'".$end."'");
        if (!isset($carId[0]->car_id))
            return http_response_code(404);
        $data = array(
            "car_id" => $carId[0]->car_id,
            "user_id" => $this->postData['user_id'],
            "start" => $start,
            "end" => $end
        );
        $this->ReservationsModel->create($data);
        echo json_encode($data);
    }

  /**
   * permet de récuperer des statistiques
   * @return int code http de la reqêtte
   */
    public function metrics() {
        $models = $this->ModelsModel->getAll($_GET);
        $metrics = array();
        foreach ($models as $model) {
            $metrics[$model->brand]['model'][] = $model->model;
        }
        echo json_encode($metrics);
    }
}