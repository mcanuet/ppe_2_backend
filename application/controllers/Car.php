<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 15:36
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Car permet de réaliser des actions sur les voitures
 */
class Car extends CI_Controller {

    private $method;
    private $postData;
    private $token;

    public function __construct() {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('CarsModel');
        $this->load->model('UsersModel');
        $this->load->model('ModelsModel');
        $this->method = $this->input->server('REQUEST_METHOD');
        $this->postData = json_decode(trim(file_get_contents('php://input')), true);
        $this->token = $this->input->get_request_header('token', true);
    }

  /**
   * test le verb de la requêtte et utilise la methode adéquate
   * @return int code http de la reqêtte
   */
    public function index() {
        if (!in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'))) {
            return http_response_code(405);
        } else if (!addRight('cars', $this->method, $this->jwt->decode($this->token)) ||
                   !addRight('models', 'GET', $this->jwt->decode($this->token))){
          return http_response_code(403);
        } else {
          $this->{$this->method}();
        }
    }

  /**
   * retourne les verbs possibles pour les requêtes
   */
    private function OPTIONS(){
        echo json_encode(array("allow methods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')));
    }

  /**
   * permet de récupérer un ou plusieurs éléments
   */
    private function GET() {
        if (isset($_GET['id'])) {
            $cars = $this->CarsModel->getOne($_GET['id']);
        } else {
            $cars = $this->CarsModel->getAll($_GET);
        }
        foreach ($cars as $car){
            $car->model = $this->ModelsModel->getOne($car->model_id)[0];
        }
//        $cars[] = array('total items' => sizeof($cars));
        echo json_encode($cars);
    }

  /**
   * permet la création d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function POST() {
        if (array_diff(array('model_id', 'plate_number', 'kilometers_count', 'last_control', 'ref_insurance', 'disponibility', 'status'), array_keys($this->postData)) != null )
            throw new Exception("bad value send: model_id, plate_number, kilometers_count, last_control, ref_insurance, disponibility, status is required", 400);
        $this->CarsModel->create($this->postData);
        return http_response_code(201);
    }

  /**
   * permet la modification d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function PUT(){
        if (!isset($this->postData['id']))
            throw new Exception('you need to set id in request body', 400);
        $this->CarsModel->update($this->postData);
        return http_response_code(204);
    }

  /**
   * permet la suppression d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function DELETE(){
        if (!isset($_GET['id']))
            throw new Exception('you need to set parameter id', 400);
        $this->CarsModel->delete($_GET['id']);
        return http_response_code(204);
    }

  /**
   * permet de récuperer des statistiques
   * @return int code http de la reqêtte
   */
    public function metrics() {
      if (!addRight('cars', 'GET', $this->jwt->decode($this->token)) ||
          !addRight('models', 'GET', $this->jwt->decode($this->token))){
        return http_response_code(403);
      }
        $cars = $this->CarsModel->getAll();
        $metrics = array();
        $metrics['total'] = 0;
        foreach ($cars as $car) {
            $car->model = $this->ModelsModel->getOne($car->model_id)[0];
            if (!isset($metrics[$car->model->brand])) {
                $metrics[$car->model->brand] = array();
                $metrics[$car->model->brand]['total'] = 0;
            }
            if (!isset($metrics[$car->model->brand][$car->model->model])) {
                $metrics[$car->model->brand][$car->model->model] = 0;
            }
            $metrics[$car->model->brand][$car->model->model] += 1;
          $metrics[$car->model->brand]['total'] += 1;
            $metrics['total'] += 1;
        }
        echo json_encode($metrics);
    }
}