<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 16:22
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Reservation permet de réaliser des actions sur les réservations
 */
class Reservation extends CI_Controller {

    private $method;
    private $postData;
    private $token;

    public function __construct() {
      parent::__construct();
      header('Content-Type: application/json');
      $this->load->model('ReservationsModel');
      $this->load->model('UsersModel');
      $this->load->model('CarsModel');
      $this->load->model('ModelsModel');
      $this->method = $this->input->server('REQUEST_METHOD');
      $this->postData = json_decode(trim(file_get_contents('php://input')), true);
      $this->token = $this->input->get_request_header('token', true);
    }

  /**
   * test le verb de la requêtte et utilise la methode adéquate
   * @return int code http de la reqêtte
   */
    public function index() {
        if (!in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'))) {
            return http_response_code(405);
        } else if (!addRight('reservations', $this->method, $this->jwt->decode($this->token)) ||
                   !addRight('users', 'GET', $this->jwt->decode($this->token)) ||
                   !addRight('cars', 'GET', $this->jwt->decode($this->token))){
          return http_response_code(403);
        } else {
          $this->{$this->method}();
        }
    }

  /**
   * permet de récupérer un ou plusieurs éléments
   */
    private function GET(){
        if (isset($_GET['id'])) {
            $reservations = $this->ReservationsModel->getOne($_GET['id']);
        } else {
            $reservations = $this->ReservationsModel->getAll($_GET);
        }
        foreach ($reservations as $reservation){
            $reservation->user = $this->UsersModel->getOne($reservation->user_id)[0];
            $reservation->car = $this->CarsModel->getOne($reservation->car_id)[0];
            $reservation->car->model = $this->ModelsModel->getOne($reservation->car->model_id)[0];
        }
        $reservations[] = array('total items' => sizeof($reservations));
        echo json_encode($reservations);
    }

  /**
   * retourne les verbs possibles pour les requêtes
   */
    private function OPTIONS(){
        echo json_encode(array("allow methods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')));
    }

  /**
   * permet la création d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function POST(){
        if (array_diff(array('car_id', 'user_id', 'start', 'end'), array_keys($this->postData)) != null )
            throw new Exception("bad value send: 'car_id', 'user_id', 'start', 'end' is required", 400);
        $this->ReservationsModel->create($this->postData);
    }

  /**
   * permet la modification d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function PUT(){
        if (!isset($this->postData['id']))
            throw new Exception('you need to set id in request body', 400);
        $this->ReservationsModel->update($this->postData);
    }

  /**
   * permet la suppression d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
    private function DELETE(){
        if (!isset($_GET['id']))
            throw new Exception('you need to set parameter id', 400);
        $this->ReservationsModel->delete($_GET['id']);
    }
}