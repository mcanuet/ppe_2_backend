<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 15:36
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Right permet de réaliser des actions sur les droits
 */
class Right extends CI_Controller {

  private $method;
  private $postData;
  private $token;

  public function __construct() {
    parent::__construct();
    header('Content-Type: application/json');
    $this->load->model('GroupModel');
    $this->load->model('RightsModel');
    $this->load->model('RightsGroupModel');
    $this->method = $this->input->server('REQUEST_METHOD');
    $this->postData = json_decode(trim(file_get_contents('php://input')), true);
    $this->token = $this->input->get_request_header('token', true);
  }

  /**
   * test le verb de la requêtte et utilise la methode adéquate
   * @return int code http de la reqêtte
   */
  public function index() {
    if (!in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'))) {
      return http_response_code(405);
    } else if (!addRight('rights', $this->method, $this->jwt->decode($this->token)) ||
      !addRight('rights_group', 'GET', $this->jwt->decode($this->token)) ||
      !addRight('groups', 'GET', $this->jwt->decode($this->token))){
      return http_response_code(403);
    } else {
      $this->{$this->method}();
    }
  }

  /**
   * retourne les verbs possibles pour les requêtes
   */
  private function OPTIONS(){
    echo json_encode(array("allow methods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')));
  }

  /**
   * permet de récupérer un ou plusieurs éléments
   */
  private function GET() {
    if (isset($_GET['id'])) {
      $rights = $this->RightsModel->getOne($_GET['id']);
    } else {
      $rights = $this->RightsModel->getAll($_GET);
    }
    echo json_encode($rights);
  }

  /**
   * permet la création d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  private function POST() {
    if (array_diff(array('method', 'table'), array_keys($this->postData)) != null ){
      throw new Exception("bad value send: method and table are required", 400);
    }
    $this->RightsRightsModel->create($this->postData);
    return http_response_code(201);
  }

  /**
   * permet la modification d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  private function PUT(){
    if (!isset($this->postData['id'])){
      throw new Exception('you need to set id in request body', 400);
    }
    $this->RightsModel->update($this->postData);
    return http_response_code(204);
  }

  /**
   * permet la suppression d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  private function DELETE(){
    if (!isset($_GET['id']))
      throw new Exception('you need to set id in parameter', 400);
    $this->RightsModel->delete($_GET['id']);
    return http_response_code(204);
  }
}