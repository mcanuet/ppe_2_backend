<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 16:42
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class User permet de réaliser des actions sur les utilisateures
 */
class User extends CI_Controller {

  private $method;
  private $postData;
  private $token;

  public function __construct() {
    parent::__construct();
    header('Content-Type: application/json');
    $this->load->model('UsersModel');
    $this->load->model('GroupModel');
    $this->load->model('RightsModel');
    $this->load->model('RightsGroupModel');
    $this->method = $this->input->server('REQUEST_METHOD');
    $this->postData = json_decode(trim(file_get_contents('php://input')), true);
    $this->token = $this->input->get_request_header('token', true);
  }

  /**
   * test le verb de la requêtte et utilise la methode adéquate
   * @return int code http de la reqêtte
   */
  public function index() {
    if (!in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'))) {
      return http_response_code(405);
    } else if (!addRight('users', $this->method, $this->jwt->decode($this->token))){
        return http_response_code(403);
    } else {
      $this->{$this->method}();
    }
  }

  /**
   * permet de récupérer un ou plusieurs éléments
   */
  public function GET() {
    if (isset($_GET['id'])) {
      $users = $this->UsersModel->getOne($_GET['id']);
    } else {
      $users = $this->UsersModel->getAll($_GET);
    }
    foreach ($users as $user) {
      $user->group = $this->GroupModel->getOne($user->id_group);
    }
    echo json_encode($users);
  }

  /**
   * permet la création d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  public function POST() {
    if (array_diff(array('email', 'password', 'phone', 'zip_code', 'town', 'street_name', 'street_number', 'id_group'), array_keys($this->postData)) != null)
      throw new Exception("bad value send: email, password, phone, zip_code, town, street_name, street_number, id_group is required", 400);
    $this->UsersModel->create($this->postData);
  }

  /**
   * permet la modification d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  public function PUT() {
    if (!isset($this->postData['id']))
      throw new Exception('you need to set id in request body', 400);
    $this->UsersModel->update($this->postData);
  }

  /**
   * permet la suppression d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  public function DELETE() {
    if (!isset($_GET['id']))
      throw new Exception('you need to set parameter id', 400);
    $this->UsersModel->delete($_GET['id']);
  }

  /**
   * retourne les verbs possibles pour les requêtes
   */
  public function OPTIONS() {
    echo json_encode(array("allow methods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')));
  }

  /**
   * permet de récupérer un token pour l'authentification deans les reqêttes
   * @return int code http de la reponse
   */
  public function login() {

    if ($this->method !== "POST")
      return http_response_code(405);
    if (array_diff(array('email', 'password'), array_keys($this->postData)) != null)
      return http_response_code(400);
    $userData = $this->UsersModel->getByCredential($this->postData['email'], sha1($this->postData['password']));
    if (!$userData){
      return http_response_code(401);
    } else {
      $userData = $userData[0];
      $userData->group = $this->GroupModel->getOne($userData->id_group)[0];
      $rights = $this->RightsGroupModel->rightsForGroup($userData->id_group);
      foreach ($rights as $right) {
        $userData->group->rights[] = $this->RightsModel->getOne($right->id_right)[0];
      }
      $token = $this->jwt->encode(array(
        'userId' => $userData->id,
        'email' => $userData->email,
        'group' => $userData->group->name,
        'rights' => $userData->group->rights,
      ), null);
      $res = array(
        'token' => $token,
        'self' => array(
          'user_id' => $userData->id,
          'email' => $userData->email,
          'phone' => $userData->phone,
          'zip_code' => $userData->zip_code,
          'town' => $userData->town,
          'street_name' => $userData->street_name,
          'street_number' => $userData->street_number,
          'group' => $userData->group->name
        )
      );
      echo json_encode($res);
    }
//    var_dump($userData);
  }

  public function info() {
    echo "<pre>";
    var_dump($this->jwt->decode($this->postData['token']));
    echo "</pre>";
  }
}