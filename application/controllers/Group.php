<?php
/**
 * Created by PhpStorm.
 * User: maxime
 * Date: 05/02/2019
 * Time: 15:36
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Group permet de réaliser des actions sur les groupes
 */
class Group extends CI_Controller {

  private $method;
  private $postData;
  private $token;

  public function __construct() {
    parent::__construct();
    header('Content-Type: application/json');
    $this->load->model('GroupModel');
    $this->load->model('RightsModel');
    $this->load->model('RightsGroupModel');
    $this->method = $this->input->server('REQUEST_METHOD');
    $this->postData = json_decode(trim(file_get_contents('php://input')), true);
    $this->token = $this->input->get_request_header('token', true);
  }

  /**
   * test le verb de la requêtte et utilise la methode adéquate
   * @return int code http de la reqêtte
   */
  public function index() {
    if (!in_array($this->method, array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'))) {
      return http_response_code(405);
    } else if (!addRight('groups', $this->method, $this->jwt->decode($this->token)) ||
               !addRight('rights', $this->method, $this->jwt->decode($this->token)) ||
               !addRight('rights_group', $this->method, $this->jwt->decode($this->token))){
      return http_response_code(403);
    } else {
      $this->{$this->method}();
    }
  }

  /**
   * retourne les verbs possibles pour les requêtes
   */
  private function OPTIONS(){
    echo json_encode(array("allow methods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')));
  }

  /**
   * permet de récupérer un ou plusieurs éléments
   */
  private function GET() {
    if (isset($_GET['id'])) {
      $groups = $this->GroupModel->getOne($_GET['id']);
    } else {
      $groups = $this->GroupModel->getAll($_GET);
    }
    foreach ($groups as $group){
      $rights = $this->RightsGroupModel->rightsForGroup($group->id);
      foreach ($rights as $right) {
        $group->rights[] = $this->RightsModel->getOne($right->id_right)[0];
      }
    }
    echo json_encode($groups);
  }

  /**
   * permet la création d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  private function POST() {
    if (array_diff(array('name', 'rights'), array_keys($this->postData)) != null )
      throw new Exception("bad value send: name and rights are required", 400);
    $id_group = $this->GroupModel->create(array('name' => $this->postData['name']))[0]->id;
    foreach ($this->postData['rights'] as $right){
      $this->RightsGroupModel->create(array('id_group' => $id_group, 'id_right' => $right));
    }
    return http_response_code(201);
  }

  /**
   * permet la modification d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  private function PUT(){
    if (!isset($this->postData['id'])){
      throw new Exception('you need to set id in request body', 400);
    }

    if ($this->postData['name']){
      $this->GroupModel->update(array('id' => $this->postData['id'], 'name' => $this->postData['name']));
    }

    if ($this->postData["rights"]){
      $actualRights = array();
      foreach ($this->RightsGroupModel->rightsForGroup($this->postData['id']) as $right) {
        $actualRights[] = $right->id_right;
      };
      $needToRemove = array_diff($actualRights, $this->postData['rights']);
      $needToAdd = array_diff($this->postData['rights'], $actualRights);
      foreach ($needToRemove as $right){
        $this->RightsGroupModel->deleteRight($right, $this->postData["id"]);
      }
      foreach ($needToAdd as $right){
        $this->RightsGroupModel->create(array("id_right" => $right, "id_group" => $this->postData['id']));
      }
    }
    return http_response_code(204);
  }

  /**
   * permet la suppression d'un élément
   * @return int code http de la reqêtte
   * @throws Exception erreur du à une mauvaise reqêtte
   */
  private function DELETE(){
    if (!isset($_GET['id']))
      throw new Exception('you need to set id in parameter', 400);
    $this->GroupModel->delete($_GET['id']);
    return http_response_code(204);
  }
}