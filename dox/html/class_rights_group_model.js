var class_rights_group_model =
[
    [ "create", "class_rights_group_model.html#a67c8b15d2856f2c4097a9db32f90b706", null ],
    [ "deleteGroup", "class_rights_group_model.html#a3779916da8b0740f3f3767e5aab98261", null ],
    [ "deleteRight", "class_rights_group_model.html#adcba230cb2cee720a8b2ee43026135b8", null ],
    [ "groupsForRight", "class_rights_group_model.html#ad62e9068e1358eae66c513874f85096d", null ],
    [ "rightsForGroup", "class_rights_group_model.html#a3fa9703bd560178725e531654683be51", null ],
    [ "updateGroup", "class_rights_group_model.html#abda6617621a877af96f79c291d632538", null ]
];