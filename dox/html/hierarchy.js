var hierarchy =
[
    [ "CI_Controller", null, [
      [ "Car", "class_car.html", null ],
      [ "Group", "class_group.html", null ],
      [ "Model", "class_model.html", null ],
      [ "Reservation", "class_reservation.html", null ],
      [ "Right", "class_right.html", null ],
      [ "User", "class_user.html", null ],
      [ "Welcome", "class_welcome.html", null ]
    ] ],
    [ "CI_Model", null, [
      [ "CarsModel", "class_cars_model.html", null ],
      [ "GroupModel", "class_group_model.html", null ],
      [ "ModelsModel", "class_models_model.html", null ],
      [ "ReservationsModel", "class_reservations_model.html", null ],
      [ "RightsGroupModel", "class_rights_group_model.html", null ],
      [ "RightsModel", "class_rights_model.html", null ],
      [ "UsersModel", "class_users_model.html", null ]
    ] ],
    [ "JWT", "class_j_w_t.html", null ]
];