# ppe_2_backend

## Installation

### Prérequis

- Serveur Web: Appache / Nginx et PHP
- Base de données Mysql
- git

### Récupération du projet

- cloner le projet via la commande `git clone https://gitlab.com/mcanuet/ppe_2_backend.git`

### mise en plcae du projet

- modifier le ficher `/application/config/database.php` avec l'addresse l'dentifiant et le mot de passe de votre base de données.
- executer les scripts sql situer dans le répertoire `/dbdump` pour créer les tables.
- Insertion de données, executer les scripts sql situer dans le répertoire `/dbdump/data`