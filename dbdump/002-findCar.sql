create function findCar(deb datetime, fin datetime, modelId int) returns int
begin
    declare car int;
    set car = (select c.id
    from cars c
       left join models m on c.model_id = m.id
       left join reservations r on c.id = r.car_id
    where m.id = modelId
      and c.id not in (
        select r.car_id
        from reservations r
        where r.start between deb and fin
           or r.end between deb and fin
           or (r.start < deb and r.end > fin)
           or (r.start < deb and r.end between deb and fin)
           or (r.start between deb and fin and r.end > fin)
        )
    limit 1);
  return car;
  end;

