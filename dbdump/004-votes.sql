create table votes
(
	id int auto_increment
		primary key,
	model_id int null,
	user_id int null,
	vote double not null,
	constraint FK_518B7ACF7975B7E7
		foreign key (model_id) references models (id),
	constraint FK_518B7ACFA76ED395
		foreign key (user_id) references users (id)
)
collate=utf8mb4_unicode_ci;

create index IDX_518B7ACF7975B7E7
	on votes (model_id);

create index IDX_518B7ACFA76ED395
	on votes (user_id);

