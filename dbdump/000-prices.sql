create table prices
(
	id int auto_increment
		primary key,
	name varchar(255) not null,
	hour_price double not null,
	day_price double not null
)
collate=utf8mb4_unicode_ci;

