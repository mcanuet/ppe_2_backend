create table reservations
(
	id int auto_increment
		primary key,
	car_id int null,
	user_id int null,
	start datetime not null,
	end datetime not null,
	constraint FK_4DA239A76ED395
		foreign key (user_id) references users (id),
	constraint FK_4DA239C3C6F69F
		foreign key (car_id) references cars (id)
)
collate=utf8mb4_unicode_ci;

create index IDX_4DA239A76ED395
	on reservations (user_id);

create index IDX_4DA239C3C6F69F
	on reservations (car_id);

