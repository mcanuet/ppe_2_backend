create table cars
(
	id int auto_increment
		primary key,
	model_id int null,
	plate_number varchar(7) not null,
	kilometers_count int not null,
	last_control datetime not null,
	ref_insurance varchar(100) not null,
	disponibility tinyint(1) not null,
	status varchar(13) not null,
	tires datetime not null,
	oil datetime not null,
	battery datetime not null,
	filters datetime not null,
	constraint UNIQ_95C71D14FCFF3785
		unique (plate_number),
	constraint FK_95C71D147975B7E7
		foreign key (model_id) references models (id)
)
collate=utf8mb4_unicode_ci;

create index IDX_95C71D147975B7E7
	on cars (model_id);

