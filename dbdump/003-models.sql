create table models
(
	id int auto_increment
		primary key,
	price_id int null,
	brand varchar(255) not null,
	model varchar(255) not null,
	fuel_type varchar(255) not null,
	image varchar(255) not null,
	description varchar(255) not null,
	places int not null,
	engine int not null,
	options varchar(255) not null,
	category varchar(255) not null,
	constraint FK_E4D63009D614C7E7
		foreign key (price_id) references prices (id)
)
collate=utf8mb4_unicode_ci;

create index IDX_E4D63009D614C7E7
	on models (price_id);

