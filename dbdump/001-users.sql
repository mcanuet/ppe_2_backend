create table users
(
	id int auto_increment
		primary key,
	email varchar(255) not null,
	password varchar(255) not null,
	phone varchar(255) not null,
	zip_code varchar(255) not null,
	town varchar(255) not null,
	street_name varchar(255) not null,
	street_number int not null,
	role varchar(255) not null
)
collate=utf8mb4_unicode_ci;

